# Yake

Yake is a task runner based on yaml files.

Use yaml files to specify Makefile-like targets and execute these
via CLI.

## Features
- YAML based syntax for task running
- Target grouping
- ENV variables per target + inheritance
- Inherits ENV variables from current environment
- Stdout / Stderr detection + formatting
- Recursive dependencies between targets
- Optional recursive Yakefile includes

## TODOs
- auto completion support for bash, zsh, ...
- ...

## Bugs

# Usage
```bash
Yake 
Tim Eggert <tim@elbart.com>
Make with yaml files

USAGE:
    yake [OPTIONS] <TARGET>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -p, --parameter <param>...    Parameters for the yake processing

ARGS:
    <TARGET>    Target to invoke
```

# Examples

For examples, please see the [Yakefile](./Yakefile) in the projects root.
