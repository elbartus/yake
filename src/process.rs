use anyhow::{anyhow, Context, Result};
use colored::Colorize;
use std::collections::HashMap;
use std::io;
use std::io::BufRead;
use std::path::Path;
use std::process::Command;
use std::process::Stdio;
use std::sync::mpsc::channel;
use std::thread::spawn;

enum StdioIndicator {
    Stdout(String),
    Stderr(String),
    StdoutEOF,
    StderrEOF,
}

pub fn run_process(
    command: &String,
    env: &HashMap<String, String>,
    yakefile_path: String,
) -> Result<()> {
    let p = Path::new(&yakefile_path);
    let cwd = p.parent().unwrap();

    if !cwd.exists() {
        return Err(anyhow!(
            "Yakefile does not exist at path: {}.",
            cwd.to_str().unwrap()
        ));
    }

    let mut process = Command::new("bash")
        .arg("-c")
        .arg(command.clone())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .envs(env)
        .current_dir(cwd)
        .spawn()
        .with_context(|| format!("failed to execute command \"{}\"", command))?;

    let (tx, rx) = channel();
    let stdout_tx = tx.clone();
    let stderr_tx = tx.clone();

    let stdout = process
        .stdout
        .take()
        .with_context(|| format!("Unable to acquire stdout"))?;
    let stderr = process
        .stderr
        .take()
        .with_context(|| format!("Unable to acquire stderr"))?;

    let t_stdout = spawn(move || {
        let stdout_buf = io::BufReader::new(stdout);
        for line in stdout_buf.lines() {
            if let Ok(l) = line {
                stdout_tx
                    .send(StdioIndicator::Stdout(l))
                    .expect("Sending stdout via channel failed!");
            }
        }

        stdout_tx
            .send(StdioIndicator::StdoutEOF)
            .expect("Sending stdout::eof via channel failed!");
    });

    let t_stderr = spawn(move || {
        let stderr_buf = io::BufReader::new(stderr);
        for line in stderr_buf.lines() {
            if let Ok(l) = line {
                stderr_tx
                    .send(StdioIndicator::Stderr(l))
                    .expect("Sending stderr via channel failed!");
            }
        }

        stderr_tx
            .send(StdioIndicator::StderrEOF)
            .expect("Sending stderr::eof via channel failed!");
    });

    let mut stdout_eof = false;
    let mut stderr_eof = false;

    while !stdout_eof || !stderr_eof {
        match rx.recv() {
            Ok(StdioIndicator::Stdout(x)) => {
                eprintln!("{}  {}", "┆".bold().green(), x);
            }
            Ok(StdioIndicator::Stderr(x)) => {
                eprintln!("{}  {}", "┆".bold().red(), x);
            }
            Ok(StdioIndicator::StdoutEOF) => stdout_eof = true,
            Ok(StdioIndicator::StderrEOF) => stderr_eof = true,
            _ => break,
        }
    }

    t_stdout.join().expect("t_stdout panicked!");
    t_stderr.join().expect("t_stderr panicked!");

    let status = process
        .wait()
        .with_context(|| format!("command wasn't running!"))?;
    if !status.success() {
        if let Some(code) = status.code() {
            return Err(anyhow!(
                "{} {}",
                "↪ Exit code was: ".bold().red(),
                code.to_string().bold().red()
            ));
        } else {
            return Err(anyhow!(
                "{}",
                "↪ Process terminated by signal!".bold().red()
            ));
        }
    }

    Ok(())
}
