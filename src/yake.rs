use std::collections::HashMap;
use std::rc::Rc;

use crate::process::run_process;
use crate::yaml::{YamlYake, YamlYakeTarget, YamlYakeTargetMeta, YamlYakeTargetType};
use anyhow::{anyhow, Context, Result};
use colored::Colorize;

type YakeEnvVars = HashMap<String, String>;

#[derive(Debug)]
pub struct Yake {
    meta: YakeMeta,
    env: Option<YakeEnvVars>,
    targets: HashMap<String, Rc<YakeTarget>>,
}

#[derive(Debug)]
pub struct YakeMeta {
    doc: String,
    version: String,
    recurse: bool,
}

#[derive(Debug)]
pub struct YakeTarget {
    pub name: String,
    meta: YakeTargetMeta,
    raw_dependencies: Option<Vec<String>>,
    env: Option<YakeEnvVars>,
    working_dir: Option<String>,
    exec: Option<Vec<String>>,
}

#[derive(Debug)]
pub struct YakeTargetMeta {
    doc: String,
    target_type: YakeTargetType,
}

#[derive(Debug)]
pub enum YakeTargetType {
    Group,
    Callable,
}

impl From<YamlYakeTargetType> for YakeTargetType {
    fn from(t: YamlYakeTargetType) -> Self {
        match t {
            YamlYakeTargetType::Callable => YakeTargetType::Callable,
            YamlYakeTargetType::Group => YakeTargetType::Group,
        }
    }
}

impl From<YamlYakeTargetMeta> for YakeTargetMeta {
    fn from(m: YamlYakeTargetMeta) -> Self {
        YakeTargetMeta {
            doc: m.doc,
            target_type: YakeTargetType::from(m.target_type),
        }
    }
}

impl Yake {
    pub fn new(
        targets: HashMap<String, Rc<YakeTarget>>,
        env: Option<HashMap<String, String>>,
        meta: YakeMeta,
    ) -> Self {
        Yake { targets, env, meta }
    }

    pub fn get_target_names(&self) -> Vec<String> {
        self.targets.iter().map(|t| t.0.clone()).collect()
    }

    pub fn get_target(&self, target_name: &String) -> Option<Rc<YakeTarget>> {
        self.targets.get(target_name).cloned()
    }

    pub fn execute_target(&self, target_name: &String) -> Result<()> {
        if !self.get_target_names().contains(target_name) {
            return Err(anyhow!("Unknown target: {}", target_name));
        }

        let target = self.get_target(target_name).unwrap();
        let dependencies = self.get_dependencies(target_name, false);

        let run_target = |target: Rc<YakeTarget>| match target.exec {
            Some(ref commands) => {
                for command in commands {
                    println!(
                        "{} {}:",
                        "↪ Executing".bold().blue(),
                        command.as_str().bold().green()
                    );

                    let env_vars = self.get_target_env_vars(target_name).unwrap_or_default();
                    let yakefile_path = match &target.working_dir {
                        Some(x) => x.clone(),
                        None => "./Yakefile".to_string(),
                    };

                    let status = run_process(command, &env_vars, yakefile_path);

                    if status.is_err() {
                        eprintln!(
                            "{}",
                            "↪ Stopping because the subprocess failed".bold().blue()
                        );
                        return false;
                    }
                }
                println!("{}", "↪ Done".bold().blue());
                return true;
            }
            None => true,
        };

        // run dependencies first
        for dep in dependencies {
            if !run_target(dep.clone()) {
                return Err(anyhow!(
                    "Stopped, because a dependency did not exit successfully."
                ));
            }
        }

        // then run the actual target
        if !run_target(target.clone()) {
            return Err(anyhow!(
                "Stopped, because the target did not exit successfully."
            ));
        }

        Ok(())
    }

    /// fetches all environment variables of the current target and it's parent targets
    pub fn get_target_env_vars(&self, target_name: &String) -> Result<HashMap<String, String>> {
        if self.get_target(&target_name).is_none() {
            return Err(anyhow!("Unknown target: {}", target_name));
        }

        let mut envs = self.env.clone().unwrap_or_default();
        let parent_targets: Vec<&str> = target_name.split(".").collect();

        // iterate over parent targets and extend the env with each of them, starting from the
        // highest hierarchy level
        for (i, _t) in parent_targets.iter().enumerate() {
            let parent_target_name = parent_targets[0..i + 1].join(".");
            let p = self
                .get_target(&parent_target_name)
                .with_context(|| format!("Unknown Target {}", parent_target_name))?;
            if p.env.is_some() {
                let env = p.env.clone();
                envs.extend(env.unwrap());
            }
        }

        let target = self.get_target(&target_name).unwrap();
        if target.env.is_some() {
            let env = target.env.clone();
            envs.extend(env.unwrap());
        }
        // envs.extend(target.env.unwrap_or_default());

        // filter blacklisted vars like PATH. If not not filtered,
        // the subprocess execution would panic due to path expansion.
        let (invalid, valid): (HashMap<&String, &String>, HashMap<&String, &String>) = envs
            .iter()
            .partition(|&k| k.0 == "TERM" || k.0 == "LANG" || k.0 == "PATH" || k.0 == "HOME");

        if invalid.len() > 0 {
            panic!(
                "{} {:?}",
                "Found invalid/forbidden env variables".bold().red(),
                invalid.keys()
            );
        }

        Ok(valid
            .iter()
            .map(|(&k, &v)| (k.clone(), v.clone()))
            .collect())
    }

    pub fn get_dependencies(&self, target_name: &String, insert: bool) -> Vec<Rc<YakeTarget>> {
        let mut dependencies = Vec::new();
        let t = self.get_target(target_name);
        match t {
            Some(t) => {
                for dep in t.raw_dependencies.as_ref().unwrap_or(&Vec::new()) {
                    dependencies.extend(self.get_dependencies(dep, true));
                }
                if insert {
                    dependencies.push(t.clone());
                }
            }
            _ => (),
        }

        dependencies
    }
}

impl YakeMeta {
    pub fn new(doc: String, recurse: bool, version: String) -> Self {
        YakeMeta {
            doc,
            recurse,
            version,
        }
    }
}

impl YakeTarget {
    pub fn new(name: &String, data: &YamlYakeTarget, path: String) -> Self {
        let d = data.clone();
        let working_dir = Some(path.clone());
        let raw_dependencies = d.meta.depends.clone();
        YakeTarget {
            name: name.clone(),
            meta: YakeTargetMeta::from(d.meta),
            raw_dependencies,
            env: d.env,
            working_dir,
            exec: d.exec,
        }
    }

    pub fn get_sub_targets(
        &self,
        data: &YamlYakeTarget,
        prefix: Option<String>,
        path: String,
    ) -> HashMap<String, Rc<YakeTarget>> {
        let mut results = HashMap::new();
        if let Some(targets) = &data.targets {
            for (name, target) in targets {
                let new_name_opt = match prefix {
                    Some(ref n) => Some(format!("{}.{}", n, name)),
                    None => Some(format!("{}.{}", self.name, name)),
                };

                let new_name = new_name_opt.as_ref().unwrap().clone();

                let nt = YakeTarget::new(new_name_opt.as_ref().unwrap(), &target, path.clone());

                if target.clone().targets.is_some() {
                    results.extend(nt.get_sub_targets(&target, new_name_opt, path.clone()));
                }

                results.insert(new_name, Rc::new(nt));
            }
        }

        results
    }
}

pub fn create(yakes: Vec<(YamlYake, String)>) -> Yake {
    let mut yake_targets = HashMap::new();
    let mut envs = HashMap::new();
    // resolve all sub-targets and store them in the hash-map
    for (y, path) in &yakes {
        for (name, target) in &y.targets {
            let t = YakeTarget::new(&name, &target, path.clone());

            yake_targets.extend(t.get_sub_targets(&target, None, path.clone()));
            yake_targets.insert(name.clone(), Rc::new(t));
        }
        envs.extend(y.env.clone().unwrap_or_default());
    }

    let mut env = None;
    if !envs.is_empty() {
        env = Some(envs.clone());
    }

    let y = Yake::new(
        yake_targets,
        env,
        YakeMeta::new("".to_string(), false, "0.0.1".to_string()),
    );

    y
}

#[cfg(test)]
mod tests {
    use serde_yaml;

    use super::*;
    use crate::yaml::YamlYakeMeta;

    fn get_yake_targets() -> HashMap<String, YamlYakeTarget> {
        let mut env = HashMap::new();
        env.insert("WEBAPP_PORT".to_string(), "6543".to_string());
        env.insert("POSTGRES_PORT".to_string(), "5432".to_string());
        let callable_target = YamlYakeTarget {
            targets: None,
            meta: YamlYakeTargetMeta {
                doc: "Test".to_string(),
                target_type: YamlYakeTargetType::Callable,
                depends: Some(vec!["base".to_string()]),
            },
            env: Some(env),
            exec: None,
        };

        let mut env_sub = HashMap::new();
        env_sub.insert("BASE".to_string(), "OVERWRITE".to_string());
        env_sub.insert("DOCKER_PORT".to_string(), "1234".to_string());
        env_sub.insert("POSTGRES_PORT".to_string(), "54322".to_string());
        let sub_target = YamlYakeTarget {
            targets: None,
            meta: YamlYakeTargetMeta {
                doc: "Subtarget".to_string(),
                target_type: YamlYakeTargetType::Callable,
                depends: Some(vec!["test".to_string()]),
            },
            env: Some(env_sub),
            exec: None,
        };

        let group_target = YamlYakeTarget {
            targets: Some([("sub".to_string(), sub_target)].iter().cloned().collect()),
            meta: YamlYakeTargetMeta {
                doc: "Grouptarget".to_string(),
                target_type: YamlYakeTargetType::Group,
                depends: None,
            },
            env: None,
            exec: None,
        };

        [
            (
                "base".to_string(),
                YamlYakeTarget {
                    targets: None,
                    meta: YamlYakeTargetMeta {
                        doc: "Base".to_string(),
                        target_type: YamlYakeTargetType::Callable,
                        depends: None,
                    },
                    env: None,
                    exec: None,
                },
            ),
            ("test".to_string(), callable_target),
            ("group".to_string(), group_target),
        ]
        .iter()
        .cloned()
        .collect()
    }

    fn get_yake() -> YamlYake {
        let targets = get_yake_targets();
        let mut env_root = HashMap::new();
        env_root.insert("BASE".to_string(), "BASEVAL".to_string());

        YamlYake {
            targets,
            env: Some(env_root),
            meta: YamlYakeMeta {
                doc: "Bla".to_string(),
                version: "1.0.0".to_string(),
                include_recursively: None,
            },
            all_targets: HashMap::new(),
        }
    }

    #[test]
    fn test_get_all_targets() {
        let yyake = get_yake();
        let yake = create(vec![(yyake, ".".to_string())]);

        let all_targets = yake.get_target_names();
        assert_eq!(all_targets.len(), 4);
    }

    #[test]
    fn test_create_yake() {
        let yyake = get_yake();

        let y = create(vec![(yyake, ".".to_string())]);
        assert!(y.get_target_names().len() == 4);
        assert!(y.get_dependencies(&"test".to_string(), false).len() == 1);
        assert!(y.get_dependencies(&"group.sub".to_string(), false).len() == 2);
        // assert!(y.get_dependencies(&"group.sub".to_string()).get(0).unwrap().name == "base".to_string());
    }

    #[test]
    fn test_get_target_by_name() {
        let yyake = get_yake();
        let yake = create(vec![(yyake, ".".to_string())]);

        assert_eq!(yake.get_target(&"group.sub".to_string()).is_some(), true);
        assert_eq!(yake.get_target(&"base".to_string()).is_some(), true);
        assert_eq!(yake.get_target(&"sub".to_string()).is_none(), true);
    }

    #[test]
    fn test_get_target_names() {
        let yyake = get_yake();
        let yake = create(vec![(yyake, ".".to_string())]);
        let names = yake.get_target_names();
        assert_eq!(names.len(), 4);
        assert_eq!(names.contains(&"group.sub".to_string()), true);
        assert_eq!(names.contains(&"base".to_string()), true);
        assert_eq!(names.contains(&"test".to_string()), true);
        assert_eq!(names.contains(&"group".to_string()), true);
    }

    #[test]
    fn test_get_dependencies_by_name() {
        let yyake = get_yake();
        let yake = create(vec![(yyake, ".".to_string())]);
        let dependencies = yake.get_dependencies(&"group.sub".to_string(), false);
        assert_eq!(dependencies.len(), 2);
        assert_eq!(dependencies[0].meta.doc, "Base".to_string());
    }

    #[test]
    fn test_get_env_vars() {
        let yyake = get_yake();
        let yake = create(vec![(yyake, ".".to_string())]);

        let envs = yake
            .get_target_env_vars(&"base".to_string())
            .unwrap_or_default();
        assert_eq!(envs.len(), 1);
        assert_eq!(envs.get("BASE").unwrap(), "BASEVAL");

        let envs = yake
            .get_target_env_vars(&"test".to_string())
            .unwrap_or_default();
        assert_eq!(envs.len(), 3);
        assert_eq!(envs.get("BASE").unwrap(), "BASEVAL");
        assert_eq!(envs.get("WEBAPP_PORT").unwrap(), "6543");
        assert_eq!(envs.get("POSTGRES_PORT").unwrap(), "5432");

        let envs = yake
            .get_target_env_vars(&"group".to_string())
            .unwrap_or_default();
        assert_eq!(envs.len(), 1);
        assert_eq!(envs.get("BASE").unwrap(), "BASEVAL");

        let envs = yake
            .get_target_env_vars(&"group.sub".to_string())
            .unwrap_or_default();
        assert_eq!(envs.len(), 3);
        assert_eq!(envs.get("BASE").unwrap(), "OVERWRITE");
        assert_eq!(envs.get("DOCKER_PORT").unwrap(), "1234");
        assert_eq!(envs.get("POSTGRES_PORT").unwrap(), "54322");
    }

    #[test]
    #[should_panic]
    fn test_get_env_vars_bad() {
        let mut env = HashMap::new();
        env.insert("WEBAPP_PORT".to_string(), "6543".to_string());
        env.insert("PATH".to_string(), "$HOME/bin:$PATH".to_string());
        let mut yyake = get_yake();
        yyake.env = Some(env);
        let yake = create(vec![(yyake, ".".to_string())]);

        let _ = yake.get_target_env_vars(&"base".to_string());
    }

    #[test]
    fn test_deserialize_yake_target_type() {
        let yml = r###"
        meta:
          doc: "Some docs"
          version: 1.0.0
        env:
          PATH: $HOME/bin:$PATH
        targets:
          base:
            meta:
              doc: "Test command"
              type: callable
            exec:
              - echo "i'm base"
          group:
            meta:
              doc: "Test command"
              type: group
        "###;

        let yake: YamlYake = serde_yaml::from_str(&yml).expect("Unable to parse");
        assert_eq!(
            yake.targets.get("base").unwrap().meta.target_type,
            YamlYakeTargetType::Callable
        );
        assert_eq!(
            yake.targets.get("group").unwrap().meta.target_type,
            YamlYakeTargetType::Group
        );
    }

    // #[test]
    // fn test_add_sub_yakes() {
    //     let yml = r###"
    //     meta:
    //       doc: "Some docs"
    //       version: 1.0.0
    //     env:
    //       PATH: $HOME/bin:$PATH
    //     targets:
    //       base:
    //         meta:
    //           doc: "Test command"
    //           type: callable
    //         exec:
    //           - echo "i'm base"
    //       group:
    //         meta:
    //           doc: "Test command"
    //           type: group
    //     "###;

    //     let subyml = r###"
    //     meta:
    //       doc: "Some docs"
    //       version: 1.0.0
    //     env:
    //       PATH: $HOME/bin:$PATH
    //     targets:
    //       base:
    //         meta:
    //           doc: "Test command overwritten"
    //           type: callable
    //         exec:
    //           - echo "i'm base, but overwritten by a sub yake"
    //       sub_base:
    //         meta:
    //           doc: "Sub: Test command"
    //           type: callable
    //         exec:
    //           - echo "i'm sub base"
    //     "###;

    //     let mut yake: YYake = serde_yaml::from_str(&yml).expect("Unable to parse");
    //     assert_eq!(
    //         yake.targets.get("base").unwrap().meta.target_type,
    //         YYakeTargetType::Callable
    //     );
    //     assert_eq!(
    //         yake.targets.get("group").unwrap().meta.target_type,
    //         YYakeTargetType::Group
    //     );

    //     let sub_yake: YYake = serde_yaml::from_str(subyml).expect("Unable to parse");
    //     assert_eq!(
    //         sub_yake.targets.get("base").unwrap().meta.target_type,
    //         YYakeTargetType::Callable
    //     );
    //     assert_eq!(
    //         sub_yake.targets.get("sub_base").unwrap().meta.target_type,
    //         YYakeTargetType::Callable
    //     );

    //     let path = "foo/Yakefile".to_string();
    //     yake.add_sub_yake(sub_yake, path);
    //     assert_eq!(
    //         yake.targets.get("base").unwrap().meta.target_type,
    //         YYakeTargetType::Callable
    //     );
    //     assert_eq!(
    //         yake.targets.get("base").unwrap().meta.doc,
    //         "Test command overwritten"
    //     );
    //     assert_eq!(
    //         yake.targets.get("sub_base").unwrap().meta.target_type,
    //         YYakeTargetType::Callable
    //     );
    // }
}
