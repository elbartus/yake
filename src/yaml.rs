use serde::de::Error;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;
use std::result;

use anyhow::{anyhow, Context, Result};
use serde_yaml;
use walkdir::{DirEntry, WalkDir};

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct YamlYake {
    /// Meta data
    pub meta: YamlYakeMeta,
    /// Environment variables
    pub env: Option<HashMap<String, String>>,
    /// Main targets
    pub targets: HashMap<String, YamlYakeTarget>,
    /// Normalized, flattened map of all targets.
    /// Not deserialized from yaml.
    #[serde(skip)]
    pub all_targets: HashMap<String, YamlYakeTarget>,
}

/// Contains meta data for the yake object.
///
/// All fields (doc, version) are required. Parsing
/// fails in case values are missing in the yaml data.
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct YamlYakeMeta {
    /// Documentation information
    pub doc: String,
    /// Version information
    pub version: String,
    /// Include Yakefiles of subfolders
    pub include_recursively: Option<bool>,
}

/// Contains meta data for a yake target.
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct YamlYakeTargetMeta {
    /// Documentation information
    pub doc: String,
    /// Type of the target, deserialized from `target`
    #[serde(rename = "type", default)]
    pub target_type: YamlYakeTargetType,
    /// List of dependent targets
    pub depends: Option<Vec<String>>,
}

/// Defines a yake target. Can have sub-targets.
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct YamlYakeTarget {
    /// Target meta data
    pub meta: YamlYakeTargetMeta,
    /// Subordinate targets
    pub targets: Option<HashMap<String, YamlYakeTarget>>,
    /// List of environment variables
    pub env: Option<HashMap<String, String>>,
    /// List of commands to execute
    /// Will only be executed for `TargetType::Cmd`
    pub exec: Option<Vec<String>>,
}

// Custom deserialization via:
// https://github.com/serde-rs/serde/issues/1019#issuecomment-322966402
/// Defines the different target types.
#[derive(Debug, PartialEq, Clone)]
pub enum YamlYakeTargetType {
    /// A Group has no own commands, just sub-targets.
    Group,
    /// A Callable has no sub-targets, just commands.
    Callable,
}

/// Implements custom serde serializer for the YakeTargetType
impl Serialize for YamlYakeTargetType {
    fn serialize<S>(&self, serializer: S) -> result::Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(match *self {
            YamlYakeTargetType::Group => "group",
            YamlYakeTargetType::Callable => "callable",
        })
    }
}

/// Sets the default value for YakeTargetType to `Callable`
impl Default for YamlYakeTargetType {
    fn default() -> Self {
        YamlYakeTargetType::Callable
    }
}

/// Implements custom serde deserializer for the YakeTargetType
impl<'de> Deserialize<'de> for YamlYakeTargetType {
    fn deserialize<D>(deserializer: D) -> result::Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        match s.as_str() {
            "group" => Ok(YamlYakeTargetType::Group),
            "callable" => Ok(YamlYakeTargetType::Callable),
            _ => Err(D::Error::custom(format!("unknown target type '{}'", s))),
        }
    }
}

pub fn load_yml_from_file(filename: &str) -> Result<YamlYake> {
    let mut f = File::open(filename)?;
    let mut contents = String::new();

    f.read_to_string(&mut contents)
        .context("Error while reading file.")?;

    if let Ok(y) = serde_yaml::from_str(&contents) {
        Ok(y)
    } else {
        Err(anyhow!("Unable to parse YAML"))
    }
}

fn find_yakefiles(directory: &str) -> Result<Vec<DirEntry>> {
    let mut files = Vec::new();

    fn is_yakefile_or_dir(entry: &DirEntry) -> bool {
        entry
            .file_name()
            .to_str()
            .map(|s| s == "Yakefile" || entry.path().is_dir())
            .unwrap_or(false)
    }

    WalkDir::new(directory)
        .min_depth(2)
        .max_depth(10)
        .into_iter()
        .filter_entry(|e| is_yakefile_or_dir(e))
        .filter_map(|v| v.ok())
        .for_each(|v| {
            if v.path().is_file() {
                files.push(v)
            }
        });

    Ok(files)
}

pub fn load_yml_from_subdirs(directory: &str) -> Result<Vec<(YamlYake, String)>> {
    let files = find_yakefiles(directory);
    let mut yakes = Vec::new();

    for entry in files.unwrap() {
        yakes.push((
            load_yml_from_file(entry.path().to_str().unwrap())?,
            entry.path().to_str().unwrap().to_string(),
        ));
    }

    Ok(yakes)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_yakefiles() {
        let dir = ".";

        let files = find_yakefiles(dir);
        assert_eq!(files.unwrap().len(), 1);
    }

    #[test]
    fn test_load_yml_from_subdirs() {
        let dir = ".";

        let sub_yakes = load_yml_from_subdirs(dir);
        assert_eq!(sub_yakes.unwrap().len(), 1);
    }
}
