//! Yake
//! ``make`` with yaml files.
//!
//! Use yaml files to specify Makefile-like targets and execute these via CLI.
extern crate serde;
extern crate structopt;
#[macro_use]
extern crate serde_derive;
extern crate anyhow;
extern crate colored;
extern crate serde_yaml;
extern crate walkdir;

mod args;
mod process;
pub mod yake;
mod yaml;

use anyhow::{Context, Result};
use std::collections::HashMap;
use std::env;
use std::process::exit;
use structopt::StructOpt;
use yaml::{load_yml_from_file, load_yml_from_subdirs};

#[derive(Debug, StructOpt)]
#[structopt(name = "yake", about = "Match lines from one file in another file.")]
pub struct Opt {
    /// TARGET to execute
    pub target: String,

    /// Additional parameters
    #[structopt(short, long)]
    pub params: Option<Vec<String>>,
}

#[derive(Debug, PartialEq)]
pub struct YakeArgs {
    pub target: String,
    pub params: HashMap<String, String>,
}

pub fn get_cli_args() -> YakeArgs {
    let opt = Opt::from_args();

    let mut args = YakeArgs {
        target: opt.target,
        params: HashMap::new(),
    };

    if let Some(parameter_values) = opt.params {
        for param in parameter_values {
            match param.trim().split("=").collect::<Vec<&str>>().as_slice() {
                [first, last] => args.params.insert(first.to_string(), last.to_string()),
                _ => None,
            };
        }
    }

    args
}

fn main() -> Result<()> {
    let yake_args = get_cli_args();
    let mut yakes = vec![(load_yml_from_file("Yakefile")?, "./Yakefile".to_string())];
    let path = env::current_dir().unwrap();

    match yakes.get(0).unwrap().0.meta.include_recursively {
        Some(true) => yakes.extend(load_yml_from_subdirs(path.to_str().unwrap()).unwrap()),
        _ => (),
    };

    let y = yake::create(yakes);

    match y.get_target(&yake_args.target) {
        None => {
            eprintln!(
                "Unknown target: '{}' Available targets are: {:?}",
                yake_args.target,
                y.get_target_names()
            );
            exit(1);
        }
        _ => (),
    };

    y.execute_target(&yake_args.target)
        .with_context(|| format!("Execution of target: {} failed.", &yake_args.target))?;

    Ok(())
}
